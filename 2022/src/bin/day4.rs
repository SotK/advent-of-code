// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::cmp::{max, min};
use std::fs;

use itertools::Itertools;

struct Area {
    start: u64,
    end: u64,
}

impl Area {
    fn new(s: &str) -> Self {
        let split = s.split("-").collect_vec();
        Self {
            start: split[0].parse::<u64>().unwrap(),
            end: split[1].parse::<u64>().unwrap(),
        }
    }

    fn contains(&self, other: &Self) -> bool {
        (self.start <= other.start && self.end >= other.end)
            || (other.start <= self.start && other.end >= self.end)
    }

    fn overlaps(&self, other: &Self) -> bool {
        max(self.start, other.start) <= min(self.end, other.end)
    }
}

fn part1(input: &str) -> u64 {
    input
        .trim()
        .lines()
        .filter(|pair| {
            let areas = pair.split(",").map(Area::new).collect_vec();
            areas[0].contains(&areas[1])
        })
        .collect_vec()
        .len() as u64
}

fn part2(input: &str) -> u64 {
    input
        .trim()
        .lines()
        .filter(|pair| {
            let areas = pair.split(",").map(Area::new).collect_vec();
            areas[0].overlaps(&areas[1])
        })
        .collect_vec()
        .len() as u64
}

fn main() {
    let input = fs::read_to_string("data/day4.txt").unwrap();
    println!("Day 4 part 1: {}", part1(input.as_str()));
    println!("Day 4 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 2)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 4)
    }
}
