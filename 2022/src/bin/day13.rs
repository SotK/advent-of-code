// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{cmp::Ordering, fs};

use itertools::Itertools;
use serde_json::Value;

fn compare(left: Value, right: Value) -> Ordering {
    let mut result = Ordering::Equal;
    result = match left {
        Value::Number(l) => match right {
            Value::Number(r) => l.as_u64().cmp(&r.as_u64()),
            Value::Array(r) => {
                let l_vec = vec![Value::from(l)];
                let n = l_vec.len().min(r.len());
                for i in 0..n {
                    result = compare(l_vec[i].clone(), r[i].clone());
                    if result != Ordering::Equal {
                        break;
                    }
                }
                if result == Ordering::Equal {
                    result = l_vec.len().cmp(&r.len());
                }
                result
            }
            _ => Ordering::Greater,
        },
        Value::Array(l) => match right {
            Value::Number(r) => {
                let r_vec = vec![Value::from(r)];
                let n = l.len().min(r_vec.len());
                for i in 0..n {
                    result = compare(l[i].clone(), r_vec[i].clone());
                    if result != Ordering::Equal {
                        break;
                    }
                }
                if result == Ordering::Equal {
                    result = l.len().cmp(&r_vec.len());
                }
                result
            }
            Value::Array(r) => {
                let n = l.len().min(r.len());
                for i in 0..n {
                    result = compare(l[i].clone(), r[i].clone());
                    if result != Ordering::Equal {
                        break;
                    }
                }
                if result == Ordering::Equal {
                    result = l.len().cmp(&r.len());
                }
                result
            }
            _ => Ordering::Greater,
        },
        _ => Ordering::Greater,
    };
    result
}

fn part1(input: &str) -> u64 {
    input
        .split("\n\n")
        .enumerate()
        .map(|(i, pair)| {
            let (left_str, right_str) = pair.trim().lines().collect_tuple().unwrap();
            let left: Value = serde_json::from_str(left_str).unwrap();
            let right: Value = serde_json::from_str(right_str).unwrap();
            (i, compare(left, right))
        })
        .filter(|(_, ordered)| *ordered == Ordering::Less)
        .map(|v| v.0 as u64 + 1)
        .sum::<u64>()
}

fn part2(input: &str) -> usize {
    let mut all_packets = input
        .lines()
        .filter(|line| !line.trim().is_empty())
        .map(|line| serde_json::from_str::<Value>(line.trim()).unwrap())
        .collect_vec();

    let start = serde_json::from_str::<Value>("[[2]]").unwrap();
    let end = serde_json::from_str::<Value>("[[6]]").unwrap();
    all_packets.push(start.clone());
    all_packets.push(end.clone());

    all_packets
        .into_iter()
        .sorted_by(|a, b| compare(a.clone(), b.clone()))
        .enumerate()
        .filter(|(_, val)| val == &start || val == &end)
        .map(|v| v.0 + 1)
        .product()
}

fn main() {
    let input = fs::read_to_string("data/day13.txt").unwrap();
    println!("Day 13 part 1: {}", part1(input.as_str()));
    println!("Day 13 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]    
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 13)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 140)
    }
}
