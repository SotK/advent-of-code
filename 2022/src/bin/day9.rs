// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{collections::HashSet, fs};

use itertools::Itertools;

#[derive(Clone, Copy, Default, Hash, PartialEq, Eq)]
struct Coordinate {
    x: i32,
    y: i32,
}

impl Coordinate {
    fn new(x: i32, y: i32) -> Self {
        Coordinate { x, y }
    }

    fn is_touching(&self, other: &Self) -> bool {
        let dx = self.x - other.x;
        let dy = self.y - other.y;
        dx.abs() <= 1 && dy.abs() <= 1
    }

    fn direction_to(&self, other: &Self) -> Self {
        Coordinate::new(other.x - self.x, other.y - self.y)
    }

    fn normalized(&self) -> Self {
        let x = if self.x != 0 {
            self.x / self.x.abs()
        } else {
            0
        };
        let y = if self.y != 0 {
            self.y / self.y.abs()
        } else {
            0
        };
        Coordinate::new(x, y)
    }
}

#[derive(Clone)]
struct Knot {
    coord: Coordinate,
    history: HashSet<Coordinate>,
}

impl Knot {
    fn new(x: i32, y: i32) -> Knot {
        let coord = Coordinate::new(x, y);
        let history = HashSet::from([coord]);
        Knot { coord, history }
    }

    fn move_in_direction(&mut self, direction: &str) -> () {
        match direction {
            "R" => self.coord = Coordinate::new(self.coord.x + 1, self.coord.y),
            "L" => self.coord = Coordinate::new(self.coord.x - 1, self.coord.y),
            "U" => self.coord = Coordinate::new(self.coord.x, self.coord.y + 1),
            "D" => self.coord = Coordinate::new(self.coord.x, self.coord.y - 1),
            _ => {}
        };
        self.history.insert(self.coord);
    }

    fn move_towards(&mut self, other: &Knot) {
        if !self.coord.is_touching(&other.coord) {
            let movement = self.coord.direction_to(&other.coord).normalized();
            self.coord = Coordinate::new(self.coord.x + movement.x, self.coord.y + movement.y);
            self.history.insert(self.coord);
        }
    }
}

impl Default for Knot {
    fn default() -> Knot {
        Knot::new(0, 0)
    }
}

fn part1(input: &str) -> usize {
    let mut head: Knot = Default::default();
    let mut tail: Knot = Default::default();

    for line in input.trim().lines() {
        let (direction, count_str) = line.split(" ").collect_tuple().unwrap();
        let count = count_str.parse::<u64>().unwrap();

        for _ in 0..count {
            head.move_in_direction(direction);
            tail.move_towards(&head);
        }
    }
    tail.history.len()
}

fn part2(input: &str) -> usize {
    let mut rope: [Knot; 10] = Default::default();

    for line in input.trim().lines() {
        let (direction, count_str) = line.split(" ").collect_tuple().unwrap();
        let count = count_str.parse::<u64>().unwrap();

        for _ in 0..count {
            rope[0].move_in_direction(direction);
            let mut prev = rope[0].clone();
            for knot in rope.iter_mut().skip(1) {
                knot.move_towards(&prev);
                prev = knot.clone();
            }
        }
    }
    rope.last().unwrap().history.len()
}

fn main() {
    let input = fs::read_to_string("data/day9.txt").unwrap();
    println!("Day 9 part 1: {}", part1(input.as_str()));
    println!("Day 9 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
";

    static LONG_ROPE: &str = "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 13)
    }

    #[test]
    fn test_coordinate_touching() {
        let a = Coordinate::new(0, 0);
        let b = Coordinate::new(1, 1);
        let c = Coordinate::new(2, 1);
        let d = Coordinate::new(0, 2);

        assert!(a.is_touching(&b));
        assert!(b.is_touching(&c));
        assert!(!a.is_touching(&c));
        assert!(!a.is_touching(&d));
    }

    #[test]
    fn test_rope_move() {
        let mut head = Knot::new(0, 0);

        head.move_in_direction("U");
        assert!(head.coord == Coordinate::new(0, 1));
        head.move_in_direction("L");
        assert!(head.coord == Coordinate::new(-1, 1));
        head.move_in_direction("D");
        assert!(head.coord == Coordinate::new(-1, 0));
        head.move_in_direction("R");
        assert!(head.coord == Coordinate::new(0, 0));
        assert!(head.history.len() == 4)
    }

    #[test]
    fn test_rope_move_towards() {
        let mut head = Knot::new(0, 0);
        let mut tail = Knot::new(0, 0);

        head.move_in_direction("U");
        tail.move_towards(&head);
        assert!(tail.coord == Coordinate::new(0, 0));

        head.move_in_direction("U");
        tail.move_towards(&head);
        assert!(tail.coord == Coordinate::new(0, 1));

        assert!((tail.history.len() == 2));
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 1);
        assert_eq!(part2(&LONG_ROPE), 36);
    }
}
