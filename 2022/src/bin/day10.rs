// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{fs, str::FromStr};

struct Command {
    modifiers: Vec<i32>,
}

#[derive(Debug)]
struct ParseCommandErr;

impl FromStr for Command {
    type Err = ParseCommandErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let modifiers = if s.starts_with("addx") {
            vec![0, s.split(" ").last().unwrap().parse::<i32>().unwrap()]
        } else {
            vec![0]
        };
        Ok(Command { modifiers })
    }
}

fn iter_cycles(input: &str) -> impl Iterator<Item = i32> + '_ {
    input
        .trim()
        .lines()
        .map(|l| l.parse::<Command>().unwrap())
        .flat_map(|c| c.modifiers.into_iter())
}

fn part1(input: &str) -> i32 {
    let mut register_values: Vec<i32> = vec![0];
    let mut x = 1;

    for modifier in iter_cycles(input) {
        register_values.push(x);
        x += modifier;
    }

    [20i32, 60i32, 100i32, 140i32, 180i32, 220i32]
        .into_iter()
        .map(|i| i * register_values[i as usize])
        .sum()
}

fn part2(input: &str) -> String {
    let mut register_values: Vec<i32> = vec![0];
    let mut pixels = ["."; 240];
    let mut x = 1;

    for (cycle, modifier) in iter_cycles(input).enumerate() {
        register_values.push(x);
        let pixel_x = (cycle as i32) % 40;
        if x.abs_diff(pixel_x) <= 1 {
            pixels[cycle] = "#";
        }
        x += modifier;
    }

    pixels
        .chunks(40)
        .map(|row| {
            row.into_iter()
                .fold(String::new(), |acc, s| acc + s.to_owned())
        })
        .fold(String::new(), |acc, line| acc + &line + "\n")
}

fn main() {
    let input = fs::read_to_string("data/day10.txt").unwrap();
    println!("Day 10 part 1: {}", part1(input.as_str()));
    println!("Day 10 part 2: \n{}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 13140)
    }

    #[test]
    fn test_part_2() {
        let expected = "##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
"
        .to_owned();
        assert_eq!(part2(&LINES), expected)
    }
}
