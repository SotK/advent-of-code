// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{collections::HashSet, fs};

use itertools::Itertools;

fn part1(input: &str) -> usize {
    let mut i = 0;
    for (a, b, c, d) in input.trim().chars().enumerate().tuple_windows() {
        let set = HashSet::from([a.1, b.1, c.1, d.1]);
        if set.len() == 4 {
            i = d.0;
            break;
        }
    }
    i + 1
}

fn part2(input: &str) -> usize {
    let input_vec = input.trim().chars().enumerate().collect_vec();

    let mut i = 0;
    for slice in input_vec.windows(14).into_iter() {
        let set: HashSet<char> = slice.into_iter().map(|t| t.1).collect();
        if set.len() == 14 {
            i = slice[slice.len() - 1].0;
            break;
        }
    }
    i + 1
}

fn main() {
    let input = fs::read_to_string("data/day6.txt").unwrap();
    println!("Day 6 part 1: {}", part1(input.as_str()));
    println!("Day 6 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 7)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 19)
    }
}
