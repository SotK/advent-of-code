// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{collections::HashSet, fs};

use itertools::Itertools;

fn str_to_char_set(to_convert: &str) -> HashSet<char> {
    to_convert.chars().collect()
}

fn char_to_priority(c: char) -> u64 {
    match c as u8 {
        x @ b'a'..=b'z' => (x - b'a' + 1) as u64,
        x @ b'A'..=b'Z' => (x - b'A' + 27) as u64,
        _ => 0,
    }
}

fn part1(input: &str) -> u64 {
    input
        .trim()
        .lines()
        .map(|rucksack| {
            let (first, second) = rucksack.split_at(rucksack.len() / 2);
            let first_set = str_to_char_set(first);
            let second_set = str_to_char_set(second);
            first_set
                .intersection(&second_set)
                .copied()
                .map(char_to_priority)
                .sum::<u64>()
        })
        .sum()
}

fn part2(input: &str) -> u64 {
    input
        .trim()
        .lines()
        .chunks(3)
        .into_iter()
        .map(|chunk| {
            chunk
                .map(str_to_char_set)
                .reduce(|acc, next| acc.intersection(&next).copied().collect())
                .unwrap()
                .into_iter()
                .map(char_to_priority)
                .sum::<u64>()
        })
        .sum()
}

fn main() {
    let input = fs::read_to_string("data/day3.txt").unwrap();
    println!("Day 3 part 1: {}", part1(input.as_str()));
    println!("Day 3 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 157)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 70)
    }
}
