// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::fs;

fn part1(input: &str) -> u64 {
    input.trim().lines().fold(0, |total, round| {
        // Round total = outcome score + shape score
        total
            + match round.trim() {
                "A X" => 3 + 1,
                "A Y" => 6 + 2,
                "A Z" => 0 + 3,
                "B X" => 0 + 1,
                "B Y" => 3 + 2,
                "B Z" => 6 + 3,
                "C X" => 6 + 1,
                "C Y" => 0 + 2,
                "C Z" => 3 + 3,
                _ => 0,
            }
    })
}

fn part2(input: &str) -> u64 {
    input.trim().lines().fold(0, |total, round| {
        // X = Lose, Y = Draw, Z = Win
        total
            + match round.trim() {
                "A X" => 0 + 3,
                "B X" => 0 + 1,
                "C X" => 0 + 2,
                "A Y" => 3 + 1,
                "B Y" => 3 + 2,
                "C Y" => 3 + 3,
                "A Z" => 6 + 2,
                "B Z" => 6 + 3,
                "C Z" => 6 + 1,
                _ => 0,
            }
    })
}

fn main() {
    let input = fs::read_to_string("data/day2.txt").unwrap();
    println!("Day 2 part 1: {}", part1(input.as_str()));
    println!("Day 2 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "A Y
B X
C Z
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 15)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 12)
    }
}
