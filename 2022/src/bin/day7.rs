// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::HashMap;
use std::fs;
use std::path::PathBuf;

use itertools::Itertools;

struct Node {
    children: Vec<PathBuf>,
    is_dir: bool,
    name: String,
    size: usize,
}

impl Node {
    fn new(name: &str, is_dir: bool, size: Option<usize>) -> Self {
        Self {
            children: vec![],
            is_dir,
            name: name.to_owned(),
            size: size.unwrap_or(0),
        }
    }

    fn from_str(s: &str) -> Self {
        let split: Vec<&str> = s.split(" ").collect();
        Self {
            children: vec![],
            is_dir: split[0] == "dir",
            name: split[1].to_owned(),
            size: split[0].parse().unwrap_or(0),
        }
    }

    fn get_size(&self, filesystem: &HashMap<PathBuf, Node>) -> usize {
        if !self.is_dir {
            return self.size;
        }
        self.children
            .iter()
            .map(|path| filesystem.get(path).unwrap().get_size(filesystem))
            .sum()
    }
}

struct Command {
    command: String,
    target: Option<String>,
}

impl Command {
    fn from_str(s: &str) -> Self {
        let split: Vec<String> = s.split(" ").map(|w| w.to_owned()).collect();
        Self {
            command: split[1].clone(),
            target: split.get(2).cloned(),
        }
    }
}

fn parse_tree(output: &str) -> HashMap<PathBuf, Node> {
    let mut filesystem: HashMap<PathBuf, Node> = HashMap::new();
    let mut cwd = PathBuf::from("/");

    let root = Node::new("/", true, None);
    filesystem.insert(cwd.clone(), root);

    for line in output.trim().lines().skip(1) {
        if line.starts_with("$") {
            let cmd = Command::from_str(line);
            if cmd.command.as_str() == "cd" {
                let target = cmd.target.unwrap();
                if target.as_str() == ".." {
                    cwd.pop();
                } else {
                    cwd.push(target)
                }
            }
        } else {
            let node = Node::from_str(line);
            let parent = filesystem.get_mut(&cwd).unwrap();
            let mut path = cwd.clone();
            path.push(&node.name);

            parent.children.push(path.clone());
            filesystem.insert(path, node);
        }
    }
    filesystem
}

fn part1(input: &str) -> usize {
    let tree = parse_tree(input);

    tree.values()
        .into_iter()
        .filter(|n| n.is_dir && n.get_size(&tree) <= 100000)
        .map(|n| n.get_size(&tree))
        .sorted()
        .sum()
}

const DISK_SIZE: usize = 70000000;
const UPDATE_SIZE: usize = 30000000;

fn part2(input: &str) -> usize {
    let tree = parse_tree(input);
    let root = tree.get(&PathBuf::from("/")).unwrap();
    let required_space = UPDATE_SIZE - (DISK_SIZE - root.get_size(&tree));

    tree.values()
        .into_iter()
        .filter(|n| n.is_dir && n.get_size(&tree) >= required_space)
        .map(|n| n.get_size(&tree))
        .min()
        .unwrap()
}

fn main() {
    let input = fs::read_to_string("data/day7.txt").unwrap();
    println!("Day 7 part 1: {}", part1(input.as_str()));
    println!("Day 7 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k    
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 95437)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 24933642)
    }
}
