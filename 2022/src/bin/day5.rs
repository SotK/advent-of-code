// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::fmt::Display;
use std::fs;

use itertools::Itertools;
use regex::Regex;

#[derive(Clone, Debug)]
struct Stack {
    crates: Vec<char>,
}

impl Stack {
    fn new() -> Self {
        Self { crates: vec![] }
    }

    fn add_crate(&mut self, c: char) -> () {
        self.crates.push(c);
    }

    fn remove_crate(&mut self) -> char {
        // Lets assume for now that the input doesn't try to move crates from empty stacks
        self.crates.pop().unwrap()
    }

    fn add_crates(&mut self, new_crates: Vec<char>) -> () {
        self.crates.extend(new_crates)
    }
    fn remove_n_crates(&mut self, n: usize) -> Vec<char> {
        self.crates.split_off(self.crates.len() - n)
    }

    fn get_top(&self) -> char {
        self.crates[self.crates.len() - 1]
    }
}

impl Display for Stack {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.crates)
    }
}

struct Instruction {
    from: usize,
    to: usize,
    count: u64,
}

impl Instruction {
    fn new(from: usize, to: usize, count: u64) -> Self {
        Self { from, to, count }
    }

    fn perform(&self, stacks: &mut Vec<Stack>) -> () {
        for _ in 0..self.count {
            let c = stacks[self.from].remove_crate();
            stacks[self.to].add_crate(c);
        }
    }

    fn perform_batched(&self, stacks: &mut Vec<Stack>) -> () {
        let crates = stacks[self.from].remove_n_crates(self.count as usize);
        stacks[self.to].add_crates(crates);
    }
}

fn parse_initial_state(map: &str) -> Vec<Stack> {
    map.lines()
        .rev()
        .enumerate()
        .fold(vec![], |mut acc, (i, line)| {
            if i == 0 {
                line.chars()
                    .chunks(4)
                    .into_iter()
                    .map(|_| Stack::new())
                    .collect_vec()
            } else {
                line.chars()
                    .chunks(4)
                    .into_iter()
                    .enumerate()
                    .map(|(stack_id, mut chunk)| {
                        let c = chunk.nth(1).unwrap();
                        if c != ' ' {
                            acc[stack_id].add_crate(c);
                        }
                        acc[stack_id].clone()
                    })
                    .collect_vec()
            }
        })
}

fn parse_instruction_set(instructions: &str) -> Vec<Instruction> {
    let re = Regex::new(r"move (?P<count>\d+) from (?P<from>\d{1}) to (?P<to>\d{1})").unwrap();
    re.captures_iter(instructions)
        .map(|captures| {
            Instruction::new(
                captures
                    .name("from")
                    .unwrap()
                    .as_str()
                    .parse::<usize>()
                    .unwrap()
                    - 1,
                captures
                    .name("to")
                    .unwrap()
                    .as_str()
                    .parse::<usize>()
                    .unwrap()
                    - 1,
                captures.name("count").unwrap().as_str().parse().unwrap(),
            )
        })
        .collect_vec()
}

fn part1(input: &str) -> String {
    let parts = input.split("\n\n").collect_vec();

    let mut stacks = parse_initial_state(parts[0]);
    let instructions = parse_instruction_set(parts[1]);
    for i in instructions {
        i.perform(&mut stacks);
    }

    stacks.into_iter().fold(String::from(""), |mut acc, stack| {
        acc.push(stack.get_top());
        acc
    })
}

fn part2(input: &str) -> String {
    let parts = input.split("\n\n").collect_vec();

    let mut stacks = parse_initial_state(parts[0]);
    let instructions = parse_instruction_set(parts[1]);
    for i in instructions {
        i.perform_batched(&mut stacks);
    }

    stacks.into_iter().fold(String::from(""), |mut acc, stack| {
        acc.push(stack.get_top());
        acc
    })
}

fn main() {
    let input = fs::read_to_string("data/day5.txt").unwrap();
    println!("Day 5 part 1: {}", part1(input.as_str()));
    println!("Day 5 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES).as_str(), "CMZ")
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES).as_str(), "MCD")
    }
}
