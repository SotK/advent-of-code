// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{collections::HashSet, convert::Infallible, fs, str::FromStr};

use itertools::Itertools;
use regex::Regex;

fn manhattan_distance(a: (i64, i64), b: (i64, i64)) -> i64 {
    (a.0.abs_diff(b.0) + a.1.abs_diff(b.1)) as i64
}

struct Sensor {
    position: (i64, i64),
    range: i64,
    beacon: (i64, i64),
}

impl Sensor {
    fn intersection_range(&self, y: i64) -> Option<(i64, i64)> {
        if y > self.position.1 + self.range || y < self.position.1 - self.range {
            return None;
        }

        if y <= self.position.1 {
            // 0,0 is top-left, so this is the top two range edges
            let edges = [
                (
                    // left to top
                    (self.position.0 - self.range, self.position.1),
                    (self.position.0, self.position.1 - self.range),
                ),
                (
                    // top to right
                    (self.position.0, self.position.1 - self.range),
                    (self.position.0 + self.range, self.position.1),
                ),
            ];
            edges
                .into_iter()
                .map(|((sx, sy), (ex, ey))| {
                    let gradient = (sy - ey) / (sx - ex);
                    let intercept = sy - gradient * sx;
                    (y - intercept) / gradient
                })
                .collect_tuple()
        } else {
            // bottom two range edges
            let edges = [
                (
                    // left to top
                    (self.position.0 - self.range, self.position.1),
                    (self.position.0, self.position.1 + self.range),
                ),
                (
                    // top to right
                    (self.position.0, self.position.1 + self.range),
                    (self.position.0 + self.range, self.position.1),
                ),
            ];
            edges
                .into_iter()
                .map(|((sx, sy), (ex, ey))| {
                    let gradient = (ey - sy) / (ex - sx);
                    let intercept = sy - gradient * sx;
                    (y - intercept) / gradient
                })
                .collect_tuple()
        }
    }
}

impl FromStr for Sensor {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"[ a-zA-z]* x=(?P<sensor_x>-?[0-9]+), y=(?P<sensor_y>-?[0-9]+): [a-z ]* x=(?P<beacon_x>-?[0-9]+), y=(?P<beacon_y>-?[0-9]+)").unwrap();
        let captures = re.captures(s).unwrap();
        let position = (
            captures.name("sensor_x").unwrap().as_str().parse().unwrap(),
            captures.name("sensor_y").unwrap().as_str().parse().unwrap(),
        );
        let beacon = (
            captures.name("beacon_x").unwrap().as_str().parse().unwrap(),
            captures.name("beacon_y").unwrap().as_str().parse().unwrap(),
        );
        let range = manhattan_distance(position, beacon);

        Ok(Self {
            position,
            range,
            beacon,
        })
    }
}

struct SensorMap {
    sensors: Vec<Sensor>,
    beacons: HashSet<(i64, i64)>,
    bounds: ((i64, i64), (i64, i64)),
}

impl FromStr for SensorMap {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let sensors = s
            .trim()
            .lines()
            .map(Sensor::from_str)
            .map(Result::unwrap)
            .collect_vec();
        let mut beacons = HashSet::new();
        let mut bounds = ((0, 0), (0, 0));
        for sensor in &sensors {
            beacons.insert(sensor.beacon);
            if sensor.position.0 - sensor.range < bounds.0 .0 {
                bounds.0 .0 = sensor.position.0 - sensor.range;
            } else if sensor.position.0 + sensor.range > bounds.0 .1 {
                bounds.0 .1 = sensor.position.0 + sensor.range;
            }
            if sensor.position.1 - sensor.range < bounds.1 .0 {
                bounds.1 .0 = sensor.position.1 - sensor.range;
            } else if sensor.position.1 + sensor.range > bounds.1 .1 {
                bounds.1 .1 = sensor.position.1 + sensor.range;
            }
        }
        Ok(Self {
            sensors,
            beacons,
            bounds,
        })
    }
}

impl SensorMap {
    fn in_range(&self, point: (i64, i64)) -> bool {
        let mut result = false;
        if self.beacons.contains(&point) {
            return false;
        }
        for sensor in self.sensors.iter() {
            if manhattan_distance(sensor.position, point) <= sensor.range {
                result = true;
                break;
            }
        }
        result
    }

    fn sensor_intersections(&self, y: i64) -> Vec<(i64, i64)> {
        self.sensors
            .iter()
            .map(|sensor| sensor.intersection_range(y))
            .filter(|result| result != &None)
            .map(Option::unwrap)
            .collect_vec()
    }
}

fn part1(input: &str, y: i64) -> i64 {
    let sensor_map: SensorMap = input.parse().unwrap();
    let x_range = sensor_map.bounds.0 .0..=sensor_map.bounds.0 .1;

    x_range.fold(0, |acc, x| {
        if sensor_map.in_range((x, y)) {
            acc + 1
        } else {
            acc
        }
    })
}

fn part2(input: &str, max_coord: i64) -> i64 {
    // for each line in the range
    //    intersect that line with each sensor
    //    order intersection points to find gaps
    //
    // Sort intersections in asc order
    // For each pair of intersections:
    //     Check for overlap
    //     If no overlap, beacon location found
    let sensor_map: SensorMap = input.parse().unwrap();
    let mut gaps = vec![];
    for y in 0..=max_coord {
        let intersections = sensor_map.sensor_intersections(y);

        let segments = intersections
            .into_iter()
            .sorted_by(|a, b| a.0.cmp(&b.0))
            .fold(
                vec![],
                |mut contiguous_segments: Vec<(i64, i64)>, segment| {
                    let mut current = contiguous_segments.pop().unwrap_or(segment);
                    if segment.0 < current.0 {
                        // should never happen
                        println!("  badly ordered segments");
                    } else if segment.0 <= current.1 + 1 && segment.1 > current.1 {
                        // there's an overlap, extend the current segment
                        current.1 = segment.1;
                    } else if segment.0 > current.1 {
                        // starting a new segment
                        contiguous_segments.push(current);
                        current = segment;
                    }
                    contiguous_segments.push(current);
                    contiguous_segments
                },
            );

        if segments.len() > 1 {
            gaps.extend(
                segments
                    .into_iter()
                    .tuple_windows()
                    .map(|(a, _)| (a.1 + 1, y)),
            );
        }
    }
    let location = gaps
        .into_iter()
        .filter(|(x, y)| x >= &0 && x <= &max_coord && y >= &0 && y <= &max_coord)
        .next()
        .unwrap();
    
    (location.0 * 4000000) + location.1
}

fn main() {
    let input = fs::read_to_string("data/day15.txt").unwrap();
    println!("Day 15 part 1: {}", part1(input.as_str(), 2000000));
    println!("Day 15 part 2: {}", part2(input.as_str(), 4000000));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3";

    #[test]
    fn test_parse_sensors() {
        let sensor_map: SensorMap = LINES.parse().unwrap();
        assert_eq!(sensor_map.beacons.len(), 6);
        assert_eq!(sensor_map.sensors.len(), 14);
        assert_eq!(sensor_map.bounds, ((-8, 28), (-10, 26)));
    }

    #[test]
    fn test_sensor_intersection() {
        let s = "Sensor at x=8, y=7: closest beacon is at x=2, y=10";
        let sensor: Sensor = s.parse().unwrap();
        assert_eq!(sensor.intersection_range(0), Some((6, 10)));
        assert_eq!(sensor.intersection_range(7), Some((-1, 17)));
        assert_eq!(sensor.intersection_range(12), Some((4, 12)));
        assert_eq!(sensor.intersection_range(-10), None);
        assert_eq!(sensor.intersection_range(1000), None);
    }

    #[test]
    fn test_part_1() {
        assert_eq!(part1(LINES, 10), 26)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(LINES, 20), 56000011)
    }
}
