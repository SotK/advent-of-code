// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::fs;

use itertools::Itertools;

fn sum_highest_n(input: &str, n: usize) -> u64 {
    input
        .trim()
        .lines()
        .group_by(|&count| count.trim().is_empty())
        .into_iter()
        .filter(|(empty, _)| !*empty)
        .map(|(_, counts)| {
            counts
                .into_iter()
                .fold(0, |total, s| total + s.parse::<u64>().unwrap_or(0))
        })
        .sorted_by(|a, b| b.cmp(a))
        .take(n)
        .sum()
}

fn main() {
    let input = fs::read_to_string("data/day1.txt").unwrap();
    println!("Day 1 part 1: {}", sum_highest_n(input.as_str(), 1));
    println!("Day 1 part 2: {}", sum_highest_n(input.as_str(), 3));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
";

    #[test]
    fn test_part_1() {
        assert_eq!(sum_highest_n(&LINES, 1), 24000)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(sum_highest_n(&LINES, 3), 45000)
    }
}
