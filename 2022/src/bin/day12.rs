// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{collections::HashMap, fs};

use itertools::Itertools;

fn get_neighbours((i, j): (usize, usize), max_i: usize, max_j: usize) -> Vec<(usize, usize)> {
    let mut ret = vec![];
    if i > 0 {
        ret.push((i - 1, j));
    }
    if i < max_i {
        ret.push((i + 1, j));
    }
    if j > 0 {
        ret.push((i, j - 1));
    }
    if j < max_j {
        ret.push((i, j + 1));
    }
    ret
}

fn get_distance(a: (usize, usize), b: (usize, usize)) -> usize {
    a.0.abs_diff(b.0) + a.1.abs_diff(b.1)
}

fn get_dh_weight(map: &Vec<Vec<u8>>, from: (usize, usize), to: (usize, usize)) -> usize {
    let from_height = map[from.0][from.1];
    let to_height = map[to.0][to.1];

    if to_height > from_height {
        0
    } else if to_height == from_height {
        1
    } else {
        1 + (from_height - to_height) as usize
    }
}

fn parse_map(input: &str) -> (Vec<Vec<u8>>, (usize, usize), (usize, usize)) {
    let mut start = (0, 0);
    let mut end = (0, 0);
    (
        input
            .trim()
            .lines()
            .enumerate()
            .map(|(i, line)| {
                line.chars()
                    .enumerate()
                    .map(|(j, c)| {
                        if c == 'S' {
                            start = (i, j);
                            'a' as u8
                        } else if c == 'E' {
                            end = (i, j);
                            'z' as u8
                        } else {
                            c as u8
                        }
                    })
                    .collect_vec()
            })
            .collect_vec(),
        start,
        end,
    )
}

fn find_path(
    map: &Vec<Vec<u8>>,
    start: (usize, usize),
    end: (usize, usize),
) -> Vec<(usize, usize)> {
    let max_i = map.len() - 1;
    let max_j = map[0].len() - 1;
    let mut open_set: Vec<(usize, usize)> = vec![];
    let mut came_from: HashMap<(usize, usize), (usize, usize)> = HashMap::new();
    let mut weight_to: HashMap<(usize, usize), usize> = HashMap::new();
    let mut est_weights: HashMap<(usize, usize), usize> = HashMap::new();
    let mut path: Vec<(usize, usize)> = vec![];

    // Add starting cell to everything
    open_set.push(start);
    weight_to.insert(start, 0);
    est_weights.insert(start, 0);

    while !open_set.is_empty() {
        // Get the most promising cell we know about
        let mut current = open_set
            .iter()
            .sorted_by(|a, b| {
                let est_a = est_weights.get(a).unwrap();
                let est_b = est_weights.get(b).unwrap();
                est_a.cmp(est_b)
            })
            .next()
            .copied()
            .unwrap();
        let weight = weight_to[&current];

        // Update the open set to not include that cell.
        // Note: a better way here would be a priority queue, but I'm too lazy to learn
        // to use the most obvious looking crate for that.
        open_set = open_set.into_iter().filter(|c| *c != current).collect();

        // If we've reached the destination, use `came_from` to reconstruct the optimum path.
        if current == end {
            path.push(current);
            let mut next = came_from.remove(&current);
            while next != None {
                match next {
                    Some(coord) => {
                        path.push(coord);
                        current = coord;
                    }
                    None => {}
                }
                next = came_from.remove(&current);
            }
            break;
        }

        // Check if the path to each valid neighbour is the best path currently known to
        // that coordinate.
        for n in get_neighbours(current, max_i, max_j)
            .into_iter()
            .filter(|n| map[n.0][n.1] <= map[current.0][current.1] + 1)
        {
            // Weight is the total Manhattan distance from the start, plus a modifier based on the altitude difference
            let n_weight = weight + get_distance(current, n) + get_dh_weight(&map, current, n);

            // Heuristic for expected total path weight is the weight to get to this point, plus the minimum possible
            // extra weight, just the Manhattan distance from here to the end.
            // Note: Maybe including the expected best-case altitude modifier would be a better heuristic here?
            let est_total_weight = n_weight + get_distance(n, end);

            // If this is the best path we know about to this neighbour, update the hashmaps to use this path
            if n_weight < *weight_to.get(&n).unwrap_or(&usize::MAX) {
                came_from.insert(n.clone(), current.clone());
                weight_to.insert(n.clone(), n_weight);
                est_weights.insert(n.clone(), est_total_weight);
                if !open_set.contains(&n) {
                    open_set.push(n);
                }
            }
        }
    }
    path
}

fn part1(input: &str) -> usize {
    let (map, start, end) = parse_map(input);
    let path = find_path(&map, start, end);
    path.len() - 1
}

fn part2(input: &str) -> usize {
    let (map, _, end) = parse_map(input);
    let starting_points = map.clone().into_iter().enumerate().map(|(i, row)| {
        row.into_iter().enumerate().filter_map(|(j, height)| {
            if height == 'a' as u8 {
                Some((i, j))
            } else {
                None
            }
        }).collect_vec()
    }).flatten().collect_vec();

    println!("Finding paths for {} starting points", starting_points.len());
    starting_points.into_iter().map(|coord| find_path(&map, coord, end).len()).filter(|i| *i > 0).min().unwrap() - 1
}

fn main() {
    let input = fs::read_to_string("data/day12.txt").unwrap();
    println!("Day 12 part 1: {}", part1(input.as_str()));
    println!("Day 12 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi
";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 31)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 29)
    }
}
