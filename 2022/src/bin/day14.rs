// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{collections::HashSet, fs, str::FromStr, string::ParseError};

use itertools::Itertools;

struct Terrain {
    rocks: HashSet<(i32, i32)>,
    min_x: i32,
    max_x: i32,
    max_y: i32,
}

impl FromStr for Terrain {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut rocks = HashSet::new();
        let mut min_x = i32::MAX;
        let mut max_x = 0;
        let mut max_y = 0;

        for formation in s.trim().lines() {
            for (a_str, b_str) in formation.split(" -> ").tuple_windows() {
                let a: (i32, i32) = a_str
                    .split(",")
                    .map(|c| c.parse::<i32>().unwrap())
                    .collect_tuple()
                    .unwrap();
                let b: (i32, i32) = b_str
                    .split(",")
                    .map(|c| c.parse::<i32>().unwrap())
                    .collect_tuple()
                    .unwrap();

                let start_x = a.0.min(b.0);
                let end_x = a.0.max(b.0);
                let start_y = a.1.min(b.1);
                let end_y = a.1.max(b.1);

                for x in start_x..=end_x {
                    if x < min_x {
                        min_x = x;
                    }
                    if x > max_x {
                        max_x = x;
                    }
                    for y in start_y..=end_y {
                        if y > max_y {
                            max_y = y;
                        }
                        rocks.insert((x, y));
                    }
                }
            }
        }

        Ok(Self {
            rocks,
            min_x,
            max_x,
            max_y,
        })
    }
}

impl Terrain {
    fn is_blocked(&self, coordinate: &(i32, i32), floor_level: Option<i32>) -> bool {
        match floor_level {
            Some(level) => coordinate.1 == level || self.rocks.contains(coordinate),
            None => self.rocks.contains(coordinate),
        }
    }

    fn add_sand(&mut self, sand: (i32, i32)) {
        self.rocks.insert(sand);
    }

    fn is_out_of_bounds(&self, coordinate: &(i32, i32), floor_level: Option<i32>) -> bool {
        match floor_level {
            Some(n) => coordinate.1 > n,
            None => {
                coordinate.0 <= self.min_x
                    || coordinate.0 >= self.max_x
                    || coordinate.1 >= self.max_y
            }
        }
    }
}

fn simulate_falling_sand(
    terrain: &mut Terrain,
    source: (i32, i32),
    floor_level: Option<i32>,
) -> usize {
    let mut sand = source;
    let mut settled_count = 0;
    while !terrain.is_out_of_bounds(&sand, floor_level) && !terrain.is_blocked(&source, floor_level)
    {
        let next_coords = [
            (sand.0, sand.1 + 1),
            (sand.0 - 1, sand.1 + 1),
            (sand.0 + 1, sand.1 + 1),
        ];

        // If all options are blocked, sand is settled
        if next_coords
            .into_iter()
            .map(|c| terrain.is_blocked(&c, floor_level))
            .all(|v| v)
        {
            terrain.add_sand(sand);
            settled_count += 1;
            sand = source;
        } else {
            sand = next_coords
                .into_iter()
                .filter(|c| !terrain.is_blocked(c, floor_level))
                .next()
                .unwrap();
        }
    }
    settled_count
}

fn part1(input: &str) -> usize {
    let mut terrain: Terrain = input.parse().unwrap();
    simulate_falling_sand(&mut terrain, (500, 0), None)
}

fn part2(input: &str) -> usize {
    let mut terrain: Terrain = input.parse().unwrap();
    let floor_level = Some(terrain.max_y + 2);
    simulate_falling_sand(&mut terrain, (500, 0), floor_level)
}

fn main() {
    let input = fs::read_to_string("data/day14.txt").unwrap();
    println!("Day 14 part 1: {}", part1(input.as_str()));
    println!("Day 14 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9";

    #[test]
    fn test_terrain_parser() {
        let terrain: Terrain = LINES.parse().unwrap();
        assert_eq!(terrain.rocks.len(), 20)
    }

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 24)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 93)
    }
}
