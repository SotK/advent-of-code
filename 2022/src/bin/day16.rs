// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{
    collections::{HashMap, HashSet, VecDeque},
    convert::Infallible,
    fs,
    str::FromStr,
};

use itertools::Itertools;
use regex::Regex;

#[derive(Clone, Debug)]
struct Valve {
    name: String,
    rate: i32,
    neighbours: Vec<String>,
}

impl FromStr for Valve {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"Valve (?P<name>[A-Z][A-Z]) has flow rate=(?P<rate>[0-9]+); tunnels? leads? to valves? (?P<neighbours>[A-Z, ]+)").unwrap();
        let captures = re.captures(s).unwrap();
        Ok(Valve {
            name: captures.name("name").unwrap().as_str().to_owned(),
            rate: captures.name("rate").unwrap().as_str().parse().unwrap(),
            neighbours: captures
                .name("neighbours")
                .unwrap()
                .as_str()
                .split(", ")
                .map(|s| s.to_owned())
                .collect(),
        })
    }
}

fn parse_valves(input: &str) -> HashMap<String, Valve> {
    input
        .trim()
        .lines()
        .map(|l| {
            let valve = l.parse::<Valve>().unwrap();
            (valve.name.clone(), valve)
        })
        .collect()
}

fn path_between(a: &Valve, b: &Valve, valves: &HashMap<String, Valve>) -> Vec<String> {
    let mut path = vec![];
    let mut potentials = VecDeque::from([a.name.clone()]);
    let mut parents: HashMap<String, String> = HashMap::new();
    let mut scores = HashMap::from([(a.name.clone(), 0)]);

    while potentials.len() > 0 {
        let mut current = potentials.pop_front().unwrap();
        if current == b.name {
            let mut next = parents.remove(&current);
            while next != None {
                match next {
                    Some(node) => {
                        path.push(node.clone());
                        current = node;
                    }
                    None => {}
                }
                next = parents.remove(&current);
            }
            break;
        }

        let valve = valves.get(&current).unwrap();
        let score = scores.get(&current).unwrap_or(&i32::MAX).clone();

        for neighbour in valve.neighbours.iter() {
            if !parents.contains_key(neighbour) {
                potentials.push_back(neighbour.clone());
                if scores.get(neighbour).unwrap_or(&i32::MAX).clone() > score + 1 {
                    parents.insert(neighbour.clone(), current.clone());
                    scores.insert(neighbour.clone(), score + 1);
                }
            }
        }
    }
    path
}

fn calculate_shortest_paths(
    node_map: &HashMap<String, Valve>,
) -> HashMap<(String, String), Vec<String>> {
    node_map
        .iter()
        .flat_map(|(name, node)| {
            node_map
                .iter()
                .filter(|(other_name, _)| name != *other_name)
                .map(|(other_name, other)| {
                    (
                        (name.clone(), other_name.clone()),
                        path_between(node, other, &node_map),
                    )
                })
                .collect_vec()
        })
        .collect()
}

fn calculate_best_route(
    node_map: &HashMap<String, Valve>,
    path_map: &HashMap<(String, String), Vec<String>>,
    start: &String,
    minutes: i32,
    used_nodes: &HashSet<String>,
) -> (i32, Vec<String>) {
    // Function to return the best route from any given node
    //
    // For the given node, calculate the best route for each child, and pick
    // the best option.
    //
    // Calculate this by recursively calling this function, until the node
    // has no unvisited children.
    fn do_calculation(
        node_map: &HashMap<String, Valve>,
        path_map: &HashMap<(String, String), Vec<String>>,
        path_so_far: &mut Vec<String>,
        closed_valves: Vec<String>,
        node: &String,
        remaining: i32,
    ) -> (i32, Vec<String>) {
        // We've arrived at a valve that can be opened, add it to the path so far
        path_so_far.push(node.clone());

        // Get the valve we're at, so we can figure out how much pressure it releases
        let valve = node_map.get(node).unwrap();

        // Get the list of valves that are still open
        let next_closed_valves = closed_valves
            .iter()
            .cloned()
            .filter(|n| n != node)
            .collect_vec();

        // If there's nowhere to go, we must have opened all the valves in this path
        // so just return the total pressure release from opening this valve
        if next_closed_valves.len() == 0 {
            (valve.rate * remaining, vec![node.clone()])
        }
        // Else, pick the next destination based on the expected total pressure release
        // from going that way
        else {
            let mut best = (0, None, vec![]);

            for n in next_closed_valves.iter() {
                let distance = path_map.get(&(node.clone(), n.clone())).unwrap().len() as i32;

                // If going to the path will take us past the 30 minute deadline,
                // don't even think about it.
                let time_left = remaining - distance - 1;
                if time_left < 0 {
                    continue;
                }

                // We don't want to modify our current path, we're speculating on one of many
                // possible futures here
                let mut speculative_path = path_so_far.clone();

                // Calculate the potential score for this next step
                let (score, path) = do_calculation(
                    node_map,
                    path_map,
                    &mut speculative_path,
                    next_closed_valves.clone(),
                    &n,
                    time_left,
                );

                // If this potential score is the best we've found, track it
                if score > best.0 {
                    best = (score, Some(n), path);
                }
            }

            // Total pressure released by opening this valve then taking the best
            // subsequent path is
            (
                (valve.rate * remaining) + best.0,
                [node.clone()].into_iter().chain(best.2).collect_vec(),
            )
        }
    }

    // Initial set of closed valves is all the valves with positive flow rate
    // minus the pre-used valves
    let closed_valves = node_map
        .iter()
        .filter(|(name, valve)| valve.rate != 0 && !used_nodes.contains(*name))
        .sorted_by(|a, b| a.1.rate.cmp(&b.1.rate))
        .map(|(n, _)| n.clone())
        .collect_vec();

    let mut path_so_far = vec![];
    do_calculation(
        node_map,
        path_map,
        &mut path_so_far,
        closed_valves,
        start,
        minutes,
    )
}

fn part1(input: &str) -> i32 {
    // Hash table to own the Valves and handle lookups by node name.
    let node_map = parse_valves(input);

    // Map of the shortest path from any given node to another node.
    let path_map = calculate_shortest_paths(&node_map);

    let (score, _) =
        calculate_best_route(&node_map, &path_map, &"AA".to_owned(), 30, &HashSet::new());
    score
}

fn calculate_double_route(
    node_map: &HashMap<String, Valve>,
    path_map: &HashMap<(String, String), Vec<String>>,
    start: &String,
    minutes: i32,
) -> i32 {
    // Get the set of all the non-zero valves
    let nodes: HashSet<String> = node_map
        .iter()
        .filter(|(_, v)| v.rate > 0)
        .map(|(n, _)| n.clone())
        .collect();

    // For each entry in the powerset of these valves, get the elements of the
    // original set not in the entry. The entry and the new set then make up a
    // pair of potential maps, one for you and one for the elephant.
    //
    // Calculate the best path through each side of this pair, and then find
    // the pair that produced the highest sum of best paths.
    //
    // This implies 2^n calls to `calculate_best_route`, which is O(n!)
    // in the worst case. So this might take a little while.
    nodes
        .clone()
        .into_iter()
        .powerset()
        .enumerate()
        .map(|(i, subset)| {
            println!("Routing {}/{}", i, 2_i32.pow(nodes.len() as u32));
            let you_subset: HashSet<String> = HashSet::from_iter(subset);
            let elephant_subset: HashSet<String> = nodes
                .clone()
                .into_iter()
                .filter(|n| !you_subset.contains(n))
                .collect();
            let you_best =
                calculate_best_route(node_map, path_map, start, minutes, &elephant_subset);
            let ele_best = calculate_best_route(node_map, path_map, start, minutes, &you_subset);
            let total = you_best.0 + ele_best.0;
            total
        })
        .max()
        .unwrap()
}

fn part2(input: &str) -> i32 {
    let node_map = parse_valves(input);
    let path_map = calculate_shortest_paths(&node_map);
    calculate_double_route(&node_map, &path_map, &"AA".to_owned(), 26)
}

fn main() {
    let input = fs::read_to_string("data/day16.txt").unwrap();
    println!("Day 16 part 1: {}", part1(input.as_str()));
    println!("Day 16 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(LINES), 1651)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(LINES), 1707)
    }
}
