// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::fs;

use itertools::Itertools;

fn make_tree_map(input: &str) -> Vec<Vec<u64>> {
    input
        .trim()
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| c.to_string().parse::<u64>().unwrap())
                .collect_vec()
        })
        .collect_vec()
}

fn part1(input: &str) -> u64 {
    let map = make_tree_map(input);
    map.iter()
        .enumerate()
        .map(|(row_idx, row)| {
            row.iter()
                .enumerate()
                .map(|(tree_idx, tree)| {
                    if row_idx == 0
                        || tree_idx == 0
                        || row_idx == map.len() - 1
                        || tree_idx == row.len() - 1
                    {
                        return 1;
                    }

                    let (row_before, row_after) = row.split_at(tree_idx);
                    if row_before.iter().all(|t| t < tree)
                        || row_after.iter().skip(1).all(|t| t < tree)
                    {
                        return 1;
                    }

                    let column = map.iter().map(|r| r[tree_idx]).collect_vec();
                    let (col_before, col_after) = column.split_at(row_idx);
                    if col_before.iter().all(|t| t < tree)
                        || col_after.iter().skip(1).all(|t| t < tree)
                    {
                        return 1;
                    }
                    0
                })
                .sum::<u64>()
        })
        .sum()
}

fn get_distance(tree_idx: usize, tree: &u64, trees: &[(usize, u64)]) -> u64 {
    if trees.is_empty() {
        return 0;
    }
    let mut before = true;
    if trees[0].0 > tree_idx {
        before = false;
    }
    let blockage = trees
        .into_iter()
        .filter(|(_, t)| t >= tree)
        .map(|(i, _)| i)
        .sorted_by(|a, b| if before { b.cmp(&a) } else { a.cmp(&b) })
        .next();

    if before {
        return match blockage {
            Some(i) => tree_idx - i,
            None => tree_idx,
        } as u64;
    } else {
        return match blockage {
            Some(i) => i - tree_idx,
            None => trees.len(),
        } as u64;
    }
}

fn part2(input: &str) -> u64 {
    let map = make_tree_map(input);
    map.iter()
        .enumerate()
        .map(|(row_idx, row)| {
            row.iter()
                .enumerate()
                .map(|(tree_idx, tree)| {
                    let column = map.iter().map(|r| r[tree_idx]).enumerate().collect_vec();
                    let row_ = row.iter().map(|t| *t).enumerate().collect_vec();
                    let (col_before, col_after) = column.split_at(row_idx);
                    let (row_before, row_after) = row_.split_at(tree_idx);

                    let left_dist = get_distance(tree_idx, tree, row_before);
                    let right_dist = get_distance(tree_idx, tree, &row_after[1..]);
                    let up_dist = get_distance(row_idx, tree, col_before);
                    let down_dist = get_distance(row_idx, tree, &col_after[1..]);
                    left_dist * right_dist * up_dist * down_dist
                })
                .max()
                .unwrap()
        })
        .max()
        .unwrap()
}

fn main() {
    let input = fs::read_to_string("data/day8.txt").unwrap();
    println!("Day 8 part 1: {}", part1(input.as_str()));
    println!("Day 8 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "
30373
25512
65332
33549
35390";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 21)
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 8)
    }
}
