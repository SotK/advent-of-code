// Copyright 2022 Adam Coldrick
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{
    collections::{HashMap, VecDeque},
    fs,
    str::FromStr,
};

use itertools::Itertools;

struct Monkey {
    items: VecDeque<u64>,
    operation: String,
    condition: String,
    success_target: usize,
    fail_target: usize,
    inspections: u64,
}

#[derive(Debug)]
struct ParseMonkeyErr;

impl FromStr for Monkey {
    type Err = ParseMonkeyErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut items: VecDeque<u64> = VecDeque::new();
        let mut operation = String::new();
        let mut condition = String::new();
        let mut success_target = 0;
        let mut fail_target = 0;
        for line in s.lines() {
            if line.trim().starts_with("Starting") {
                let (_, items_str) = line.split(": ").collect_tuple().unwrap();
                items = items_str.split(", ").map(|s| s.parse().unwrap()).collect()
            } else if line.trim().starts_with("Operation") {
                let (_, operation_str) = line.split(": ").collect_tuple().unwrap();
                operation = operation_str.trim().to_owned();
            } else if line.trim().starts_with("Test") {
                let (_, test_str) = line.split(": ").collect_tuple().unwrap();
                condition = test_str.trim().to_owned();
            } else if line.trim().starts_with("If true") {
                let (_, target) = line.split("throw to monkey ").collect_tuple().unwrap();
                success_target = target.trim().parse().unwrap();
            } else if line.trim().starts_with("If false") {
                let (_, target) = line.split("throw to monkey ").collect_tuple().unwrap();
                fail_target = target.trim().parse().unwrap();
            }
        }
        Ok(Monkey {
            items,
            operation,
            condition,
            success_target,
            fail_target,
            inspections: 0,
        })
    }
}

impl Monkey {
    fn get_condition_divisor(&self) -> u64 {
        let (_, divisor_str) = self.condition.split(" by ").collect_tuple().unwrap();
        divisor_str.parse().unwrap()
    }

    fn inspect_items(&mut self, worry_mod: u64) -> HashMap<usize, Vec<u64>> {
        let mut monkey_map = HashMap::new();
        monkey_map.insert(self.success_target, vec![]);
        monkey_map.insert(self.fail_target, vec![]);
        while self.items.len() > 0 {
            let item = self.items.pop_front().unwrap();
            let (_, modifier) = self.operation.split("= old ").collect_tuple().unwrap();
            let (operator, value_str) = modifier.split(" ").collect_tuple().unwrap();
            let value = value_str.parse::<u64>().unwrap_or(item);

            // Modify the worry value whilst inspecting
            let mut new_value = item;
            if operator == "*" {
                new_value *= value;
            } else if operator == "+" {
                new_value += value;
            }

            // Done inspecting, divide by worry modifier
            self.inspections += 1;
            if worry_mod == 3 {
                new_value /= worry_mod;
            } else {
                new_value %= worry_mod;
            }

            // Run the test to find the target monkey
            let divisor = self.get_condition_divisor();
            if new_value % divisor == 0 {
                monkey_map
                    .get_mut(&self.success_target)
                    .unwrap()
                    .push(new_value);
            } else {
                monkey_map
                    .get_mut(&self.fail_target)
                    .unwrap()
                    .push(new_value);
            }
        }
        monkey_map
    }
}

fn part1(input: &str) -> u64 {
    let mut monkeys = input
        .trim()
        .split("\n\n")
        .map(|s| s.parse::<Monkey>().unwrap())
        .collect_vec();

    for _ in 1..=20 {
        for i in 0..monkeys.len() {
            let monkey = &mut monkeys[i];
            let item_map = monkey.inspect_items(3);
            for key in item_map.keys() {
                for item in item_map[key].iter() {
                    monkeys[*key].items.push_back(*item);
                }
            }
        }
    }

    monkeys
        .into_iter()
        .map(|m| m.inspections)
        .sorted_by(|a, b| b.cmp(a))
        .take(2)
        .product()
}

fn part2(input: &str) -> u64 {
    let mut monkeys = input
        .trim()
        .split("\n\n")
        .map(|s| s.parse::<Monkey>().unwrap())
        .collect_vec();

    let worry_mod = monkeys.iter().map(Monkey::get_condition_divisor).product();

    for _ in 1..=10000 {
        for i in 0..monkeys.len() {
            let monkey = &mut monkeys[i];
            let item_map = monkey.inspect_items(worry_mod);
            for key in item_map.keys() {
                for item in item_map[key].iter() {
                    monkeys[*key].items.push_back(*item);
                }
            }
        }
    }

    monkeys
        .into_iter()
        .map(|m| m.inspections)
        .sorted_by(|a, b| b.cmp(a))
        .take(2)
        .product()
}

fn main() {
    let input = fs::read_to_string("data/day11.txt").unwrap();
    println!("Day 11 part 1: {}", part1(input.as_str()));
    println!("Day 11 part 2: {}", part2(input.as_str()));
}

#[cfg(test)]
mod tests {
    use super::*;

    static LINES: &str = "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
  ";

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&LINES), 10605)
    }

    static TEST_MONKEY: &str = "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3";

    #[test]
    fn test_parse_monkey() {
        let monkey: Monkey = TEST_MONKEY.parse().unwrap();
        assert_eq!(monkey.items, vec![79, 98]);
        assert_eq!(monkey.condition, "divisible by 23");
        assert_eq!(monkey.success_target, 2);
        assert_eq!(monkey.fail_target, 3);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&LINES), 2713310158)
    }
}
